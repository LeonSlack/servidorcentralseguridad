/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorcentral;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.x509.X509V1CertificateGenerator;

public class Certificado
{
    
	private X509Certificate cert;
	
	public Certificado(KeyPair pair, String name)
	{
		try
		{
			
                        cert = generateV1Certificate(pair, name);
                        
		}
		catch (Exception e)
		{
                    System.out.println(e);
		}
	}
	
	public Certificado(X509Certificate cert)
	{
		this.cert = cert;
	}
	
	
	public void saveCert(String path)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(path);
			fos.write( cert.getEncoded() );
			fos.flush();
			fos.close();
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
	}
	
	public void savePlainCert(String path)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(path);
			//fos.write( cert.getEncoded() );	
			fos.write(cert.toString().getBytes());
			fos.flush();
			fos.close();
		}
		catch (Exception ex)
		{
			System.exit(0);
		}
	}
	
	public X509Certificate getCertificado()
	{
		return this.cert;
	}
	
	public void saveCertificateToFile(String path)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(path);
			fos.write(cert.getEncoded());
			fos.flush();
			fos.close();
		}
		catch (Exception e){ }		
	}
	
	public static void loadCertificate(String path)
	{
            
	}
	
	
	private X509Certificate generateV1Certificate(KeyPair pair, String name)
	        throws InvalidKeyException, NoSuchProviderException, SignatureException, CertificateEncodingException, IllegalStateException, NoSuchAlgorithmException
    {
        // generate the certificate
        X509V1CertificateGenerator  certGen = new X509V1CertificateGenerator();

        certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
        certGen.setIssuerDN(new X500Principal("CN=ServidorCentral, OU=8voSemestre, O=Ingenieria Informatica, C=VE"));
        certGen.setNotBefore(new Date(System.currentTimeMillis() - 5000000));
        certGen.setNotAfter(new Date(System.currentTimeMillis() + 5000000));
        certGen.setSubjectDN(new X500Principal("CN=" + name + ", OU=8voSemestre, O=Ingenieria Informatica, C=VE"));
        certGen.setPublicKey(pair.getPublic());
        certGen.setSignatureAlgorithm("SHA256WithRSA"); 
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        return certGen.generateX509Certificate(pair.getPrivate());

    }
}