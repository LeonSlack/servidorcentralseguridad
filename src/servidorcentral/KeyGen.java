/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorcentral;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import static org.apache.commons.codec.binary.Base64.decodeBase64;
import static org.apache.commons.codec.binary.Base64.encodeBase64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class KeyGen
{
	private static int DATA_SIZE = 50;
	private static String MODE = "RSA";
	private static String PROVIDER = "BC";
	
	private KeyPairGenerator kg;
	private KeyPair kp;
	private PrivateKey prk;
	private PublicKey puk;
	
	private Cipher cipher;
	
	public KeyGen()
	{
		try
		{
			kg = KeyPairGenerator.getInstance(MODE);
			kg.initialize(512);
			
			cipher = Cipher.getInstance(MODE);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public KeyGen(PublicKey puk, PrivateKey prk)
	{
		try
		{
			kg = KeyPairGenerator.getInstance(MODE, PROVIDER);
			kg.initialize(512);
			
			cipher = Cipher.getInstance(MODE, PROVIDER);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		this.puk = puk;
		this.prk = prk;
	}
	
        
        public String encriptarConPublica(String s)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, puk);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        public String encriptarConPublicaExterna(String s,PublicKey p)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, p);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        public String encriptarConPrivada(String s)
        {
            		try
		{
			cipher.init(Cipher.ENCRYPT_MODE, prk);
                        byte[] encriptado = cipher.doFinal(s.getBytes());
			return new String(encodeBase64(encriptado));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
        }
        
        public String desencriptarConPrivada(String s)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, prk);
                        
                        byte[] enc =Base64.decodeBase64(s); 
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        public String desencriptarConPublica(String s)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, puk);
                        
                        byte[] enc =Base64.decodeBase64(s);
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        public String desencriptarConPublicaExterna(String s, PublicKey p)
	{
		try
		{
			
                        cipher.init(Cipher.DECRYPT_MODE, p);
                        
                        byte[] enc =Base64.decodeBase64(s);
                        byte[] des = cipher.doFinal(enc);

			return new String(des);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
        
        public String EncriptarKS(String texto,String secretKey) 
        {
            String encriptado = "";
            try 
            {

                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cip = Cipher.getInstance("DESede");
                cip.init(Cipher.ENCRYPT_MODE,key);
                byte[] plainTextBytes = texto.getBytes("utf-8");
                byte[] buf = cip.doFinal(plainTextBytes);
                byte[] base64Bytes = Base64.encodeBase64(buf);
                encriptado = new String(base64Bytes);

            } 
            catch (Exception ex) 
            {
                ex.printStackTrace();
            }
            return encriptado;
        }

        public String DesencriptarKS(String textoEncriptado,String secretKey)
        {
            String Desencriptado = "";
            try 
            {
                byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
                byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
                SecretKey key = new SecretKeySpec(keyBytes, "DESede");
                Cipher cip = Cipher.getInstance("DESede");
                cip.init(Cipher.DECRYPT_MODE, key);
                byte[] plainText = cip.doFinal(message);

                Desencriptado = new String(plainText, "UTF-8");

            } 
            catch (Exception ex) 
            {
                ex.printStackTrace();                
            }
            return Desencriptado;
        }
        	
	public Cipher getCipher()
	{
		return cipher;
	}
	
	public KeyPair getKeyPair()
	{
		return this.kp;
	}
	
	public void generateKeys()
	{
		KeyPair kp = kg.generateKeyPair();
		
		this.kp = kp;
		prk = kp.getPrivate();
		puk = kp.getPublic();
	}
	
	public PrivateKey getPrivateKey()
	{
		return prk;
	}
	
	public PublicKey getPublicKey()
	{
		return puk;
	}
	
	public static String byteArrayToString(byte [] buffer) 
	{
		return new String(buffer, StandardCharsets.UTF_8);
	}
	
}
