/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorcentral;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nupy
 */
public class escucha extends Thread {
    private static ServerSocket serverSocket;
    private static Socket clientSocket = null;
    //private List conectados=new ArrayList();
    private List <String>Peliculas=new ArrayList();
    private List <servidor>Servidores = new ArrayList();
    private static String KS;
    private static KeyGen key= new KeyGen();
    
    public escucha (Socket cliente,ServerSocket serverS)
    {
        this.clientSocket = cliente;
        this.serverSocket=serverS;
        //this.conectados=conectados;
    }
    public void addPelicula (String p)
    {
        if (Peliculas.contains(p)==false)
            Peliculas.add(p);
    };
    public void SK (String sk/*, PublicKey puk, PrivateKey prk*/)
    {
        KS=sk;
    }
    public void addServer (servidor s)
    {
        
        if(Servidores.contains(s)==false)
            Servidores.add(s);
    };
    public void run (){
        key.generateKeys();
        while (true) {
            try {
                
                clientSocket = serverSocket.accept();

                Thread t = new Thread(new CLIENTConnectionA(clientSocket));

                t.start();
               

            } catch (Exception e) {
                System.err.println(e);
            }
            
        }
    }
    
    
    public class CLIENTConnectionA implements Runnable {

        private Socket clientSocket;
        private BufferedReader in = null;

        public CLIENTConnectionA(Socket client) {
            this.clientSocket = client;
        }

        @Override
        public void run() {
            try {
                
                String log="0";
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String opcion="";
                DataOutputStream LOG = new DataOutputStream(clientSocket.getOutputStream());
                String nombre;
                String pass;
                DataInputStream is = new DataInputStream(clientSocket.getInputStream());
                String g = in.readLine();
                //System.out.println(g);
                PublicKey PublicaCliente;
                //empiezo intercambio de publicas
                if (g.contains("no")){
                try {
                    //System.out.println("entre en el trai");
                    //envio mi publica
                    ByteBuffer bb = ByteBuffer.allocate(4);
                    bb.putInt(key.getPublicKey().getEncoded().length);
                    LOG.write(bb.array());
                    LOG.write(key.getPublicKey().getEncoded());
                    LOG.flush();
                    //Recibo la clave publica del servidor                   
                    byte[] lenb = new byte[4];
                    is.read(lenb,0,4);
                    bb = ByteBuffer.wrap(lenb);
                    int len = bb.getInt();
                    byte[] servPubKeyBytes = new byte[len];
                    is.read(servPubKeyBytes);
                    X509EncodedKeySpec k = new X509EncodedKeySpec(servPubKeyBytes);
                    KeyFactory kf = KeyFactory.getInstance("RSA");
                    PublicaCliente = kf.generatePublic(k);
                    
                    LOG.writeUTF(key.encriptarConPrivada(KS));
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }   
                }            
                //
                while ( (opcion=in.readLine())!=null){
                    

                        switch (opcion){
                            case "ENTRAR":  
                                
                                nombre=key.DesencriptarKS(in.readLine(),KS);// para poder recibir NOMBRE cliente
                                pass=key.DesencriptarKS(in.readLine(),KS);// para poder recibir clave cliente
                                Usuario tem=new Usuario();
                                tem.setUSUARIO(nombre);
                                tem.setPass(pass);
                                Usuario tem2= Usuario.buscar_Usuario_obj(nombre,ServidorCentral.getUsuarios());
                                if((tem2!=null)&&(tem2.getPass().equals(pass))){
                                    log="1";
                                }
                                LOG.writeUTF(log); //envio respuesta de acceso
                                break;
                            case "INSCRIPCION":    
                                String reg="0";
                                nombre=key.DesencriptarKS(in.readLine(),KS);// para poder recibir NOMBRE cliente
                                pass=key.DesencriptarKS(in.readLine(),KS);// para poder recibir clave cliente
                                    if (Usuario.buscar_Usuario(nombre, ServidorCentral.getUsuarios())==-1)
                                    {//si no esta registrado el usuario
                                        ServidorCentral.Agregar_Usuario(nombre, pass);
                                        reg="1";
                                    }

                                LOG.writeUTF(reg); //envio respuesta de acceso
                                break;
                        //} 
                            case "SALIR":
                                break;
                            case "entro":
                               //logg escribo /in leo 
                                
                                /*Iterator i = Peliculas.listIterator();
                                while (i.hasNext())
                                {
                                    LOG.writeUTF(i.);
                                    System.out.println(i.toString());

                                }*/
                                for (int x=0;x<Peliculas.size();x++)
                                {
                                    LOG.writeUTF(Peliculas.get(x).toString());
                                    
                                }                                   
                                LOG.writeUTF("fin");
                                int indice =in.read()-1;
                                //System.out.println(indice);
                                if (indice>=0){
                                    //System.out.println(indice);
                                    LOG.writeUTF(Peliculas.get(indice));
                                    //System.out.println(Peliculas.get(indice));//recibi el indice del video
                                    //System.out.println(Servidores.size());
                                    for (int x=0;x<Servidores.size();x++)
                                    {     

                                        if (Servidores.get(x).Tienes(Peliculas.get(indice))==true)
                                        {
                                            LOG.writeUTF(Servidores.get(x).getIP());
                                            int p =Servidores.get(x).getPORT();
                                            //System.out.println(Servidores.get(x).getIP()+" "+p);

                                            LOG.writeUTF(Integer.toString(p));
                                            //le mando solo los servidores que tienen el video
                                        }
                                    }
                                    LOG.writeUTF("fin");
                                }
                                /*indice=in.read()-1;
                                System.out.println(indice+" este es el indice del servidor que elegi");
                                LOG.writeUTF(Servidores.get(indice).getIP());
                                
                                LOG.writeUTF(Integer.toString(Servidores.get(indice).getPORT()));*/
                                break;
                            /*default:
                                    switch (opcion) {

                                        case "1":
                                            DataOutputStream server1 = new DataOutputStream(clientSocket.getOutputStream());
                                            server1.writeUTF("192.168.0.101"); //aqui iria el ip del primer servidor
                                            server1.writeUTF("5555");
                                            break;
                                        case "2":
                                            DataOutputStream server2 = new DataOutputStream(clientSocket.getOutputStream());
                                            server2.writeUTF("192.168.0.100"); //aqui iria el ip del segundo servidor
                                            server2.writeUTF("6666");
                                            break;
                                        case "0":
                                            System.out.println(opcion);
                                            break;
                                        default:      
                                            System.out.println("Comando incorrecto");
                                            break;
                                    }

                                //}
                                break;*/
                        }

                       
                }
              //LOG.;
              in.close();  
                            
            } 
            catch (IOException ex) 
            {
                //Logger.getLogger(CLIENTConnectionA.class.getName()).log(Level.SEVERE, null, ex);
                //System.err.println(ex);
            }
        }
    }   
}
