/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorcentral;


import java.awt.event.*;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import javax.swing.Timer;

/**
 *
 * @author Nupy
 */
public class ServidorCentral {

    private static ServerSocket serverSocket;
    private static Socket clientSocket = null;
    private static LinkedList<Usuario> Usuarios=new LinkedList<Usuario>();
    public static LinkedList<Usuario> getUsuarios() {
        return Usuarios;
    }
    //datos para hacer peticiones a los servidores de descarga
    private static Socket Datos_Servidor;
    private static List <servidor>Servidores = new ArrayList();
    private static servidor S;
    private static servidor I;
    private static DataOutputStream ps;
    private static DataInputStream I_S;
    private static KeyGen key = new KeyGen();
    private static PublicKey puk;
    private static final String SK="ChristianLeonEduardoLorenzoSeguridadComputacional";
    //////////////////////////
    public static void Agregar_Usuario(String nombre,String pass) throws IOException {
       Usuario temp=new Usuario();
       temp.USUARIO=nombre;
       temp.pass=pass;
       Usuarios.add(temp);
       PrintWriter pw = new PrintWriter(new FileWriter("C:\\central\\users.txt",true));
       pw.println(nombre);
       pw.println(key.EncriptarKS(pass, SK));
       pw.close();
    }
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        //AGREGO LOS SERVIDORES
        key.generateKeys();
        puk = key.getPublicKey();
        S = new servidor ("192.168.0.103",6666);
        Servidores.add(S);
        /*S = new servidor ("192.168.0.104",5555);
        Servidores.add(S);*/
        //////
        ///Leo los usuarios del archivo
        FileReader f = new FileReader("C:\\central\\users.txt");
        BufferedReader b = new BufferedReader(f);
        String cadena;
        String pas;
        while((cadena= b.readLine())!=null) 
        {
            pas=b.readLine();
            Usuario temp=new Usuario();
            temp.USUARIO=cadena;
            temp.pass=key.DesencriptarKS(pas, SK);
            Usuarios.add(temp);          
        }
        b.close();
        ////

        try {
            serverSocket = new ServerSocket(4444);
            
            System.out.println("Servidor central activo");
        } catch (Exception e) {
            System.err.println("Puerto ocupado");
            System.exit(1);
        }
        escucha hiloEscuchar = new escucha (clientSocket,serverSocket);
        hiloEscuchar.start();
        hiloEscuchar.SK(SK/*,key.getPublicKey(),key.getPrivateKey()*/);
        Timer timerPelis = new Timer (60000 ,new ActionListener() 
        {
            public void actionPerformed(ActionEvent e)
            {
                Iterator <servidor> it = Servidores.iterator();
                while (it.hasNext())
                {
                    try {
                        I=it.next();
                        Datos_Servidor = new Socket(I.getIP(),I.getPORT());
                        DataOutputStream ps = new DataOutputStream(Datos_Servidor.getOutputStream());
                        //////EMPIEZO A PEDIR LAS PELICULAS DEL SERVIDOR DE DESCARGA
                        ps.writeUTF("peliculas"); 
                        //encripto con mi privada
                        //System.out.println("Pelculas del servidor: "+I.getIP());
                        I_S = new DataInputStream(Datos_Servidor.getInputStream());
                        String pelicula = key.DesencriptarKS(I_S.readUTF(),SK); //Desencripto con su publica
                        while (pelicula.matches("fin")==false){
                            if (pelicula!="fin"){
                                if(I.Tienes(pelicula)==false)
                                    I.addPelicula(pelicula);
                                hiloEscuchar.addPelicula(pelicula);
                            }
                            pelicula=key.DesencriptarKS(I_S.readUTF(),SK);
                            
                        }
                        hiloEscuchar.addServer(I);

                        ////////TENGO LAS PELICULAS EN LA LISTA
                        ////////PIDO EL NUMERO DE DESCARGAS                
                        String numDescargas = key.DesencriptarKS(I_S.readUTF(),SK);
                        I.setDescargas(Integer.parseInt(numDescargas));
                        ///////Tengo el numero de descargas
                        ///////Pido los clientes que han descargado peliculas                       
                        String Desdeip = key.DesencriptarKS(I_S.readUTF(),SK);;
                        if (Desdeip.matches("vacia")){

                        }
                        else
                            while (Desdeip.matches("fin")==false){
                                if (Desdeip!="fin"){
                                    if (I.getClientes().contains(Desdeip)==false)
                                        I.addCliente(Desdeip);
                                    //System.out.println(Desdeip);
                                }   
                                
                                Desdeip = key.DesencriptarKS(I_S.readUTF(),SK);;
                            }
                        /////Recibi los clientes que han descargado peliculas
                        
                    
                        
                        Datos_Servidor.close();
                    }
                    catch (IOException ex){
                        System.err.println("No se pudo conectar con este servidor se intentará mas tarde"+ex);
                    };
                }
            } 
        });
    timerPelis.setRepeats(true);
     
    String selection = "";
    InputStreamReader ipr = new InputStreamReader(System.in);
    BufferedReader br = new BufferedReader (ipr);
    while (true){
        System.out.println("Introduzca un comando(man para info.)");
        selection = br.readLine();
        
        //Para cada comando lo que hago es conectarme en cada servidor, pidiendole los reportes necesarios.
        switch (selection){
            case "man":
                System.out.println("Comandos disponibles:");
                System.out.println("ESTADISTICAS: Muestra las estadisticas de cada servidor.");
                System.out.println("register: registra los servidores entregando certificado en tiempo real");
                
                break;
            case "ESTADISTICAS":
                for (int x=0;x<Servidores.size();x++)
                {
                    I=Servidores.get(x);
                    System.out.println("Estadisticas del servidor: "+Servidores.get(x).getIP());
                    System.out.println("Videos disponibles:");
                    for (int y=0;y<Servidores.get(x).getPeliculas().size();y++)
                    {
                        System.out.println((y+1)+". "+Servidores.get(x).getPeliculas().get(y));
                    }
                    System.out.println("Numero de descargas: "+Servidores.get(x).GetNumDescargas());
                    System.out.println("Clientes que han descargado videos:");
                    for(int z=0;z<Servidores.get(x).getClientes().size();z++)
                    {
                        System.out.println((z+1)+". "+Servidores.get(x).getClientes().get(z));
                    }
                    System.out.println("Cantidad de archivos descargados: "+Servidores.get(x).GetNumDescargas());
                }
                
                break;
            case "register":
                Iterator <servidor> i = Servidores.iterator();
                while (i.hasNext())
                {
                    I=i.next();
                    Datos_Servidor = new Socket(I.getIP(),I.getPORT());
                    I_S = new DataInputStream(Datos_Servidor.getInputStream());
                    ps = new DataOutputStream(Datos_Servidor.getOutputStream());
                    ps.writeUTF("register");
                    System.out.println("Registrando: "+I.getIP());
                    PublicKey pu;
                    //Recibo la clave publica del servidor
                    byte[] lenb = new byte[4];
                    I_S.read(lenb,0,4);
                    ByteBuffer bb = ByteBuffer.wrap(lenb);
                    int len = bb.getInt();
                    byte[] servPubKeyBytes = new byte[len];
                    I_S.read(servPubKeyBytes);
                    X509EncodedKeySpec ks = new X509EncodedKeySpec(servPubKeyBytes);
                    KeyFactory kf = KeyFactory.getInstance("RSA");
                    pu = kf.generatePublic(ks);
                    I.setPUK(pu);
                    //envio mi publica
                    bb = ByteBuffer.allocate(4);
                    bb.putInt(key.getPublicKey().getEncoded().length);
                    ps.write(bb.array());
                    ps.write(key.getPublicKey().getEncoded());
                    ps.flush();
                    //Empiezo el compendio de ks
                    ps.writeUTF(key.encriptarConPublicaExterna(SK, I.getPUK()));
                    //
                    System.out.println("Se han intercambiado claves publicas con el servidor: "+I.getIP());
                    //Empiezo la entrega del certificado
                    Certificado c = new Certificado(key.getKeyPair(),I.getIP());
                    String fileC="C:\\central\\"+I.getIP()+".crt";
                    c.saveCert(fileC);  
                    //maneja la lectura de archivo
                    File myFile = new File(fileC);
                    byte[] mybytearray = new byte[(int) myFile.length()];

                    FileInputStream fis = new FileInputStream(myFile);
                    BufferedInputStream bis = new BufferedInputStream(fis);

                    DataInputStream dis = new DataInputStream(bis);
                    dis.readFully(mybytearray, 0, mybytearray.length);

                    //maneja los archivos que se enviaran por el socket
                    //OutputStream os = Datos_Servidor.getOutputStream();

                    //envio el nombre del archivo y su tamaño al cliente
                    //DataOutputStream dos = new DataOutputStream(os);
                    ps.writeUTF(myFile.getName());
                    ps.writeLong(mybytearray.length);
                    ps.write(mybytearray, 0, mybytearray.length);
                    fis.close();
                    ps.flush();
                    //c.savePlainCert("C:\\central\\"+I.getIP()+".crt.plano");
                    //
                    Datos_Servidor.close();
                    
                }
                timerPelis.start();
                break;                

            default:
                System.out.println("Error de comando");
        }
        
    }

    }
    
}